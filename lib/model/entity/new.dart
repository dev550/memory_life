import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'new.g.dart';

@JsonSerializable()
class News extends Equatable {
  int id;
  String title;
  @JsonKey(name: "content")
  String content;
  @JsonKey(name: "image")
  String image;
  @JsonKey(name: "datecreate")
  String date;
  @JsonKey(name: "localX")
  String localX;
  @JsonKey(name: "localY")
  String localY;
  @JsonKey(name: "activeId")
  int activeLd;
  @JsonKey(name: "feelId")
  int feelId;
  @JsonKey(name: "userId")
  int user;

  News(this.id, this.title, this.content, this.image, this.date, this.localX,
      this.localY, this.activeLd, this.feelId, this.user);

  factory News.fromJson(Map<String, dynamic> json) => _$NewsFromJson(json);

  Map<String, dynamic> toJson() => _$NewsToJson(this);

  @override
  List<Object> get props =>
      [id, title, content, image, date, localX, localY, activeLd, feelId, user];

  @override
  String toString() {
    return 'New{id: $id, title:$title,content:$content,image:$image,date:$date,localX:$localX,localY:$localY,feelId:$feelId,user:$user }';
  }
}
